import os
import logging
from PyPDF2 import PdfMerger
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas
from tempfile import mkdtemp

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

FONT_NAME = 'DejaVuSerif'
FONT_PATH = './DejaVuSerif.ttf'
FONT_SIZE = 24
PDF_EXTENSION = ".pdf"

pdfmetrics.registerFont(TTFont(FONT_NAME, FONT_PATH))


class PDFCreator:
    def __init__(self, font_name=FONT_NAME, font_size=FONT_SIZE, page_size=letter):
        self.font_name = font_name
        self.font_size = font_size
        self.page_size = page_size

    def create_title_page(self, title, output_path):
        """ Creates a title page with the specified title. """
        try:
            c = canvas.Canvas(output_path, pagesize=self.page_size)
            width, height = self.page_size

            c.setFont(self.font_name, self.font_size)
            max_width = width * 0.9
            lines = self.split_title_into_lines(title, c, max_width)

            start_y = height / 2 + (len(lines) - 1) * self.font_size / 2

            for line in lines:
                c.drawCentredString(width / 2, start_y, line)
                start_y -= self.font_size * 1.2

            c.save()
            logging.info(f"Title page created: {output_path}")
        except Exception as e:
            logging.error(f"Failed to create title page: {e}")

    def split_title_into_lines(self, title, canvas, max_width):
        """ Splits title into lines based on max width allowed. """
        words = title.split()
        lines = []
        current_line = []

        for word in words:
            test_line = ' '.join(current_line + [word])
            if canvas.stringWidth(test_line, self.font_name, self.font_size) < max_width:
                current_line.append(word)
            else:
                if current_line:
                    lines.append(' '.join(current_line))
                current_line = [word]
        if current_line:
            lines.append(' '.join(current_line))

        return lines


class PDFMergerUtility:
    def __init__(self, pdf_creator):
        self.pdf_creator = pdf_creator
        self.temp_dir = mkdtemp()

    def merge_pdfs(self, pdfs, output_path):
        merger = PdfMerger()
        temp_files = set()
        try:
            for pdf in pdfs:
                if pdf.endswith(PDF_EXTENSION):
                    merger.append(pdf)
                else:
                    temp_pdf_path = os.path.join(self.temp_dir, f"{pdf}{PDF_EXTENSION}")
                    self.pdf_creator.create_title_page(pdf, temp_pdf_path)
                    merger.append(temp_pdf_path)
                    temp_files.add(temp_pdf_path)

            merger.write(output_path)
            merger.close()
            logging.info(f"Final merged PDF created at {output_path}")
        finally:
            for temp_file in temp_files:
                os.remove(temp_file)
                logging.info(f"Temporary file {temp_file} deleted")


# Example usage
if __name__ == "__main__":
    pdfs = [
        "1) Выписка из зачётки (Диплом будет в июне)",
        "./Выписка из зачётки.pdf",
        "2) Документы, подтверждающие индивидуальные достижения абитуриента",
        "./Сертификат БВСР.pdf",
        "...",
        "3) Документы, подтверждающие наличие опыта по специфике программы",
        "./Самозанятый.pdf",
        "...",
        "4) Мотивационное письмо",
        "./Мотивационное письмо.pdf",
        "5) Эссе на заданную тему: «Ключевые направления развития Интернета: угрозы, риски и возможности»",
        "./Ключевые направления развития Интернета_ угрозы, риски и возможности.pdf",
    ]

    final_pdf_path = "./Документы для раннего поступления.pdf"
    pdf_creator = PDFCreator()
    merger = PDFMergerUtility(pdf_creator)
    merger.merge_pdfs(pdfs, final_pdf_path)
